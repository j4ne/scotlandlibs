package scotlandLibs.map;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class GameMap {

    private final int resX = 600;
    private final int resY = 600;

    private final List<Point> points = new ArrayList<>();
    private final List<Street> streets = new ArrayList<>();
    private static int pointID = 0;


    public void createPoint(Point p) {
        points.add(p);
    }

    public void createPoint(int x, int y, boolean bus, boolean subway) {
        var p = new Point(pointID++, x, y, bus, subway);
        points.add(p);
    }

    public Point getPoint(int index) {
        return points.get(index);
    }

    public void updatePoint(int index, Point p) {
        points.set(index, p);
    }

    public void updatePoint(int index, int x, int y, boolean bus, boolean subway) {
        Point p = new Point(points.get(index).getId(), x, y, bus, subway);
        updatePoint(index, p);
    }

    public void deletePoint(int index) {
        deleteRelatedStreets(index);
        points.remove(index);
    }

    private void deleteRelatedStreets(int point) {

//        Tobi Code
//        streets.stream()
//            .filter(e -> e.getStartingPoint() == index || e.getEndingPoint() == index)
//                .forEach(streets::remove);

        for (Street s : streets) {
            if (s.getStartingPoint() == point || s.getEndingPoint() == point)
                streets.remove(s);
        }
    }


    public void createStreet(Street street) {

        if ((points.stream().anyMatch(e -> e.getId() != street.getStartingPoint()))
                && (points.stream().anyMatch(e -> e.getId() != street.getEndingPoint())))
            streets.add(street);
        else
            throw new IndexOutOfBoundsException();

    }

    public void printStreets() {
        for (Point p : points) {
            System.out.println("p = " + p);
        }
    }
}