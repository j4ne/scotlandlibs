package scotlandLibs.map;

class Street {

    private int startingPoint;
    private int endingPoint;

    private boolean hasBus;
    private boolean hasSubway;

    public int getEndingPoint() {
        return endingPoint;
    }

    public void setEndingPoint(int endingPoint) {
        this.endingPoint = endingPoint;
    }

    public int getStartingPoint() {
        return startingPoint;
    }

    public void setStartingPoint(int startingPoint) {
        this.startingPoint = startingPoint;
    }

    public boolean isBus() {
        return hasBus;
    }

    public void setBus(boolean hasBus) {
        this.hasBus = hasBus;
    }

    public boolean isSubway() {
        return hasSubway;
    }

    public void setSubway(boolean hasSubway) {
        this.hasSubway = hasSubway;
    }
}
