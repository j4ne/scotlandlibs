package testing;

import scotlandLibs.map.GameMap;

public class MapTest {
    public static void main(String[] args) {
        GameMap map = new GameMap();

        map.createPoint(1, 1, true, false);
        map.createPoint(2, 2, false, false);
        map.deletePoint(0);
        map.createPoint(1, 5, true, true);
        map.updatePoint(1, 1, 4, true, true);

        map.printStreets();
    }
}
