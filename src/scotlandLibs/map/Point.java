package scotlandLibs.map;

@SuppressWarnings("unused")
public class Point {

    private final int id;
    private int pointX;
    private int pointY;

    private boolean hasBus;
    private boolean hasSubway;


    Point(int id, int x, int y, boolean bus, boolean subway) {
        this.id = id;
        this.pointX = x;
        this.pointY = y;

    }

    @Override
    public String toString() {
        return String.format("ID:%d   X:%d   Y:%d", this.id, this.pointX, this.pointY);
    }

    public int getPointX() {
        return pointX;
    }

    public void setPointX(int pointX) {
        this.pointX = pointX;
    }

    public int getPointY() {
        return pointY;
    }

    public void setPointY(int pointY) {
        this.pointY = pointY;
    }

    public boolean isHasBus() {
        return hasBus;
    }

    public void setHasBus(boolean hasBus) {
        this.hasBus = hasBus;
    }

    public boolean isHasSubway() {
        return hasSubway;
    }

    public void setHasSubway(boolean hasSubway) {
        this.hasSubway = hasSubway;
    }

    public int getId() {
        return id;
    }
}
